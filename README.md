# Imágen de Tryton con módulos específicos de Argentina

La imágen contiene los siguientes módulos de tryton-ar:

~~~text
https://github.com/tryton-ar/account_ar
https://github.com/tryton-ar/current_account
https://github.com/tryton-ar/party_ar
https://github.com/tryton-ar/account_invoice_ar
https://github.com/tryton-ar/sale_pos_ar
https://github.com/tryton-ar/bank_ar
https://github.com/tryton-ar/account_voucher_ar
https://github.com/tryton-ar/account_check_ar
https://github.com/tryton-ar/account_retencion_ar
https://github.com/tryton-ar/account_report_ar
https://github.com/tryton-ar/recover_invoice_ar
https://github.com/tryton-ar/account_inflation_adjustment_ar
https://github.com/tryton-ar/subdiario
https://github.com/tryton-ar/citi_afip
https://github.com/tryton-ar/account_move_summary
https://github.com/tryton-ar/account_invoice_ar_currency
https://github.com/tryton-ar/product_price_list_ar
https://github.com/tryton-ar/currency_ar
https://github.com/tryton-ar/account_arba
~~~

Y también módulos de gcoop:

~~~text
https://github.com/gcoop-libre/trytond-account_move_renumber
https://github.com/gcoop-libre/trytond-account_coop_ar
https://github.com/gcoop-libre/trytond-account_debt
https://github.com/gcoop-libre/trytond-account_statement_banco_credicoop
~~~
